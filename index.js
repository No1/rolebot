const fs = require('fs');
const Discord = require('discord.js');
const settings = require(`./token.json`);

const messages_file = './yason/roledict.json';
const logged_messages = get_log();

const client = new Discord.Client();
const new_coll = new Discord.Collection();

const role_admin = settings.role_name;

const min_args = 2;
const max_args = 5;
const ignore = "I will ignore this message.";
const invalid_snowflake = "Invalid Snowflake";
const already_has_role = "This entry is already present in Logged Messages";
const snowflake_regex = /^\d+$/;
const snowflake_max = "130374108079718400000"; //year 3000, jan 1, GMT
const snowflake_min_len = 15; //year 3000, jan 1, GMT


/**
 * initialization code
 */
client.on('ready', () => {
  console.log(`Logged in as ${client.user.tag}!`);
  const channel = client.channels.get(settings.role_channel_id);
  refresher(channel, null);
});

async function wait_for(ms){
  return new Promise(resolve => setTimeout(resolve, ms));
}

function get_log(){
  const collection = new Discord.Collection();
  const arrays = require(messages_file);
  for(const [key, value] of arrays)
    collection.set(key, value);

  return collection;
}

function save_log(){
  const obj = [];
  for(const kv of logged_messages.entries())
    obj.push(kv);
  const stringy = JSON.stringify(obj);
  fs.writeFileSync(messages_file, stringy);
}

function map_has(map, key)
{
  return map.has(key.toString());
}

function map_set(map, key, value)
{
  map = map.set(key.toString(), value);
  save_log();
  return map;
}

function map_delete(map, key)
{
  const ret = map.delete(key.toString());
  if(!ret) return false;
  save_log();
  return true;
}

function map_get(map, key)
{
  return map.get(key.toString());
}

async function send_delete(channel, message){
    const temp = await channel.send(message);
    temp.delete(5000);
}

function add_emoji(channel, input){
  if(map_has(logged_messages, input.key))
    {
      send_delete(channel, 'This message already has that emoji');
      return;
    }
  map_set(logged_messages, input.key, input.role_id);
}

function del_emoji(channel, input){
  map_delete(logged_messages, input.key);
}

const s1_before_s2 = (s1, s2) => {
  if(s1.length > s2.length) return false;
  else if(s1.length < s2.length) return true;
  return s1 < s2;
}

const validate_snowflake = (snowflake) => {
  if(snowflake.match(snowflake_regex)
  && s1_before_s2(snowflake, snowflake_max)
  && snowflake.length > snowflake_min_len)
  {
    return snowflake;
  }
  throw `${invalid_snowflake}:${snowflake}`;
}

const validate_message = async (channel, message_id) => {
  validate_snowflake(message_id);
  try{
    const message = await channel.fetchMessage(message_id);
    if(message.deleted)
    {
      send_delete(channel, 'The message specified has already been deleted.');
      const delete_keys = [];
      for(const key of logged_messages)
      {
        if(key.split(/,/)[0] === message_id)
          delete_keys.push(key);
      }
      for(const key of delete_keys)
        map_delete(logged_messages, key);
      if(delete_keys.length > 0)
        send_delete(channel, 'Deleting all entries with this message in Logged Messages.');

      throw 'Deleted Message';
    }
    return message;
  }catch(err){
    send_delete(channel, 'Message is not in this channel');
    throw err;
  }
}

const guild_emojis_only = 'Only Guild Emojis are allowed for now.';
const validate_emoji = (channel, emoji) => {
  const matcher = emoji.match(/:.*:(\d+)/);
  let emoji_id;
  try{
  if(matcher && matcher.length > 1)
    emoji_id = validate_snowflake(matcher[1]);
  else
    emoji_id = validate_snowflake(emoji);
  if(!channel.guild.emojis.has(emoji_id))
      throw guild_emojis_only;
  return emoji_id;

  }catch(err){
    if(err.toString() === guild_emojis_only)
      send_delete(channel, guild_emojis_only);
    else
      send_delete(channel, 'Invalid Emoji');
    throw err;
  }
}


const validate_role = (channel, role_id) => {
  const matcher = role_id.match(/<@&(\d+)>/);
  if(matcher && matcher.length > 1)
    role_id = matcher[1]
  try{
    validate_snowflake(role_id);
  }catch(err){
    throw 'Invalid Role ID';
  }
  if(!channel.guild.roles.has(role_id))
  {
    send_delete(channel, 'Invalid Role ID');
    throw 'Invalid Role ID';
  }
  return role_id;
}

async function validate_args(channel, split){
    if(split.length < min_args || split.length > max_args) return null;

    const address_me = `${client.user}` === split[0];
    if(!address_me) return null;
    if(!['addrole', 'delrole', 'refresh'].includes(split[1]))
  {
    send_delete(channel, 'Please specify an action.');
    throw 'No Action Given';
  }

  const return_object = {};
  if(split[1] === 'refresh' && split.length === 2)
  {
    return_object.action = refresher;
    return return_object;
  }

  const message = await validate_message(channel, split[2]);
  const emoji = validate_emoji(channel, split[3]);
  return_object.key = [message.id, emoji];
  return_object.message = message;

    
  if(split[1] === 'addrole' && split.length === 5)
    {
      if(map_has(logged_messages, return_object.key))
      {
        send_delete(channel, already_has_role);
        throw already_has_role;
      }
      return_object.role_id = validate_role(channel, split[4]);
      return_object.action = add_role;
    }
    else if(split[1] === 'delrole' && split.length >= 4)
    {
      return_object.action = del_role;
    }
    else throw ignore;

    return return_object;
}

async function refresher(channel, input){
  const guild_members = channel.guild.members;
  for(const [key, role_id] of logged_messages.entries())
  {
    const split = key.split(/,/);
    const message_id = split[0];
    const emoji_id = split[1];
    let message;
    try{
    message = await channel.fetchMessage(message_id);
    }catch(err)
    {
      console.log(`wasn't able to find ` + message_id);
      map_delete(logged_messages, key);
      continue;
    }
    if(message.deleted)
    {
      map_delete(logged_messages, key);
      continue;
    }
    const reaction = message.reactions.find(r => r.emoji.id === emoji_id);
    if(reaction)
    {
      const users = await reaction.fetchUsers();
      users.delete(client.user.id);

      for(const [id, user] of users.entries()){
        if(!guild_members.has(id))
          reaction.remove(id);
        else
        {
          const member = guild_members.get(id);
          if(!member.roles.has(role_id))
          {
            member.addRole(role_id);
            send_delete(channel, `Please verify that I just gave you the role you wanted, ${member.displayName}.`);
            await wait_for(1000);
          }
        }
      }
    }
  }
}

async function add_role(channel, input){
  const emoji_id = input.key[1];
  const message_reaction = await input.message.reactions.find(r => r.emoji.id === emoji_id);
  if(message_reaction)
  {
    const users = await message_reaction.fetchUsers();
    users.delete(client.user.id);
    const members = channel.guild.members;
    for(const [user_id, user] of users)
    {
      const member = members.get(user_id);
      if(member)
      {
        await member.addRole(input.role_id);
        send_delete(channel, `Please verify that I just gave you the role you wanted, ${member.displayName}.`);
      }
    }
  }
  
  await input.message.react(emoji_id);
  await wait_for(2000);
  add_emoji(channel, input);
}

async function del_role (channel, input){
  const message_reaction = await input.message.reactions.find(r => r.emoji.id === input.key[1]);

  if(!message_reaction)
  {
    await del_emoji(channel, input);
    send_delete(channel, 'For this message, no users had that reaction.');
    return;
  }
  const users = await message_reaction.fetchUsers();
  if(!users || users.size === 0)
  {
    await del_emoji(channel, input);
    send_delete(channel, 'For this message, no users had that reaction.');
    return;
  }

  for(const [user_id, user] of users)
  {
    await message_reaction.remove(user);
  }
  await wait_for(2000);
  del_emoji(channel, input);
}

client.on('raw', async packet => {
  try{
    if(!['MESSAGE_REACTION_ADD', 'MESSAGE_REACTION_REMOVE'].includes(packet.t)) return;
    if(packet.d.channel_id !== settings.role_channel_id) return;
    if(!packet.d.emoji.id) return;//assume only custom emojis or emoji with id is allowed
    const key = [packet.d.message_id, packet.d.emoji.id];
    if(!map_has(logged_messages, key)) return;

    const channel = client.channels.get(settings.role_channel_id);
    const member = channel.guild.members.get(packet.d.user_id);
    const role = map_get(logged_messages, [packet.d.message_id, packet.d.emoji.id]);

    if(packet.t === 'MESSAGE_REACTION_ADD')
    {
      member.addRole(role);
      send_delete(channel, `Please verify that I just gave you the role you wanted, ${member.displayName}.`);
    }
    else if(packet.t === 'MESSAGE_REACTION_REMOVE')
    {
      member.removeRole(role);
      send_delete(channel, `Please verify that I just removed the role you don't want anymore, ${member.displayName}.`);
    }
  }catch(err){
    console.log(err);
  }});

/**
 * Deals with a message. Namely, adds an entry to the serailzed
 * message-emoji-dictionary. For an entry to be recorded, the following
 * requirements must be followed:
 *   msg.author cannot be a bot
 *   msg.member must have role_admin role
 *   msgid must be valid
 *   usage of :emoji: in dictionary must be acceptable
 *   role ping must be valid
 *   syntax must be correct
 * Only the following two syntaxs are accepted at the moment:
 *
 * @${client.user.tag} delrole msgid :emoji:
 * or
 * @${client.user.tag} addrole msgid :emoji: role_ping
 *
 */
client.on('message', async msg => {
  try{
    if(msg.author.bot) return;
    if(!msg.member.roles.find(role => role.name === role_admin)) return;
    if(msg.channel.id !== settings.role_channel_id) return;

    const channel = msg.channel;
    const input = await validate_args(channel, msg.content.trim().split(/\s+/,max_args));
    if(!input) return;
    msg.delete(5000);
    await input.action(channel, input);

  }catch(err){
    msg.delete(5000);
    console.log(msg.content);
    console.log(err);
  }
});

client.login(settings.token);
